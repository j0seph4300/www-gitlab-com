---
layout: markdown_page
title: "Category Direction - Package Registry"
---

- TOC
{:toc}

## Package Registry

As part of our overall vision for packaging at GitLab, we want to provide a single interface for managing dependencies,
registries, and package repositories. Whether C/C++, Java, Ruby, or any other language, our vision is that you can
store your binaries and dependencies in the GitLab Package Registry. There are a few key package/language types on our radar
that we want to support as part of this category:

### Available now
- Maven helps **Java** developers to simplify the build process by providing a uniform build system to streamline development. The [GitLab Maven Repository](https://docs.gitlab.com/ee/user/packages/maven_repository/) provides a standardized method of sharing and versioning Java dependencies across projects.
- NPM allows developers to access **JavaScript** packages to help drive faster development. The [GitLab NPM Registry](https://docs.gitlab.com/ee/user/packages/npm_registry/) allows developers to publish, share and version control all of their packages right alongside their source code.
- **C/C++** developers are an important group of users who need these capabilities, and [Conan](https://conan.io) is an open-source solution that is already available and has been requested by GitLab users. The [GitLab Conan Repository](https://docs.gitlab.com/ee/user/packages/conan_repository/) enables C/C++ developers to build, publish and share all of their dependencies, all from within GitLab. 

### Coming soon
- For **C#/.NET** developers, we plan to create a private [NuGet Repository](https://gitlab.com/gitlab-org/gitlab/issues/20050) which will allow developers to publish and share .NET packages and help make developing .NET apps much easier. 
- **PHP** is a common web development language, that is used by Wordpress and other similar tools that is preferred for it's ease of use and low cost. We will support PHP developers by creating a private [Composer Repository](https://gitlab.com/gitlab-org/gitlab/issues/15886). 

### Planned
- **Python** is a versatile programming language that can be used for web development, gaming, configuring servers, performing scientific calculations and data analysis. We will empower Python developers and data scientists to publish and share their Python packages to a private, GitLab hosted [PyPi Repository](https://gitlab.com/gitlab-org/gitlab/issues/10483), right alongside their source code and pipelines. We chose [PyPi](https://pypi.org/project/pip/), as it is the standard package manager for Python.
- Linux distros depend on linux package regisitries for distribution of installable software. By supporting [**Debian**](https://gitlab.com/gitlab-org/gitlab/issues/5835) and [**RPM**](https://gitlab.com/gitlab-org/gitlab/issues/5932) packages we will cater to a big chunk of the market and allow [Systems Administrator](https://design.gitlab.com/research/personas#persona-sidney) tasks to be internalized.
- The planned [GitLab Rubygem Repository](https://gitlab.com/gitlab-org/gitlab/issues/803) offers **Ruby** developers of lower-level services to provision an easy to use, integrated solution to share and version control ruby gems in a standardized and controlled way. Being able to provision them internally sets projects up for improved privacy and pipeline build speeds.

Because there are many great solutions out there in the market for package registries, our plan is to provide a single, consistent interface to whichever package manager you're using. We want you to be able to take advantage of the great features each of these tools offer, but at the same time, we want there to be a consistent experience that's well integated with the rest of your workflow at GitLab.

- [Issue List](https://gitlab.com/groups/gitlab-org/-/issues?label_name%5B%5D=Category%3APackage+Registry)
- [Overall Vision](https://about.gitlab.com/direction/package/)
- [UX Research](https://gitlab.com/groups/gitlab-org/-/epics/593)

This page is maintained by the Product Manager for Package, Tim Rizzi ([E-mail](mailto:trizzi@gitlab.com))

## What's Next & Why

We have a lot of exciting features currently in progress. Our current plan is to add support for several new package manager formats, redesign the user interface and improve our existing integrations. 

First up is [gitlab-#20050](https://gitlab.com/gitlab-org/gitlab/issues/20050), the MVC for the GitLab NuGet Repository, which will help .NET/C# developers by allowing them to publish and share dependencies to a private, GitLab-hosted repository. 

Once the NuGet Repository is complete, we will begin working on [gitlab-#10483](https://gitlab.com/gitlab-org/gitlab/issues/10483), which will allow developers to setup a private, GitLab hosted PyPi Repository. 

## Maturity Plan

This category is currently at the "Viable" maturity level, and
our next maturity target is "Complete" (see our [definitions of maturity levels](/direction/maturity/)).
Key deliverables to achieve this are:

- Allow users to build, publish and share the most commonly used package formats, using the command line or GitLab pipelines
  - [NuGet (.NET)](https://gitlab.com/gitlab-org/gitlab/issues/20050) (In progress)
  - [PyPi (Python)](https://gitlab.com/gitlab-org/gitlab/issues/10483)(Scheduled)
  - [RubyGems](https://gitlab.com/gitlab-org/gitlab/issues/803) (Scheduled)
  - [Debian](https://gitlab.com/gitlab-org/gitlab/issues/5835) (Planned)
  - [RPM](https://gitlab.com/gitlab-org/gitlab/issues/5932) (Planned)

## Competitive Landscape

|         | GitLab | Artifactory | Nexus | GitHub | JetBrains
| ------- | ------ | ----------- | ----- | ------ | ------ |
| Conan   | ✔️ | ✔️ | ☑️ | - | - |
| Debian  | - | ✔️ | ✔️ | - | - |
| Maven   | ✔️ | ✔️ | ✔️ | ️✔️ ️| ✔️ |
| NPM     | ✔️ | ✔️ | ✔️ | ✔️ | - |
| NuGet   | - | ✔️ | ✔️ | ✔️ | - |
| PyPi    | - | ✔️ | ✔️ | - | - |
| RPM     | - | ✔️ | ✔️ | - | - |
| RubyGem | - | ✔️ | ✔️ | ✔️ | - |


☑️ _indicates support is through community plugin or beta feature_

Historically, we've provided access to the GitLab container registry for free, but limited access to package registries to our paid tier. However, in an effort to drive more community contributions, any community contributed package manager will be added to GitLab Core.

## Top Customer Success/Sales Issue(s)

- Our top customer success and sales issues are all focused on adding support for new package manager formats:
  - [gitlab-#10483](https://gitlab.com/gitlab-org/gitlab/issues/10483): PyPi Repository MVC
  - [gitlab-#803](https://gitlab.com/gitlab-org/gitlab/issues/803): RubyGems
  - [gitlab-#5835](https://gitlab.com/gitlab-org/gitlab/issues/5835): Debian
  - [gitlab-#5932](https://gitlab.com/gitlab-org/gitlab/issues/5932): RPM

## Top Customer Issue(s)

- For Maven and NPM, our top customer issues are [gitlab-#32159](https://gitlab.com/gitlab-org/gitlab/issues/32159) and [gitlab-#33685](https://gitlab.com/gitlab-org/gitlab/issues/33685) which will simplify how we enforce package scopes and naming conventions.


## Top Internal Customer Issue(s)

- The Distribution team utilizes an external tool for building Linux packages. By offering support for Debian and RPM we can begin to remove that external dependency. 
- The GitLab Distribution team also utilizes rubygems for downloading external dependencies. They would like to speed up their build times and remove their reliance on external dependencies by caching frequently used packages. This would require an integration with rubygems as well as the dependency proxy. [gitlab-#225](https://gitlab.com/gitlab-org/distribution/team-tasks/issues/225) details their needs and requirements. 
- The Release group would like to leverage the GitLab Package Registry to store release assets and make them available for discovery and download. Although we are still working through how to best implement this, [gitlab-#36133](https://gitlab.com/gitlab-org/gitlab/issues/36133) discusses the need and use cases. 

## Top Vision Item(s)

A lot of moving our vision forward is integrating MVC support for new languages and package types:

### C/C++

Our vision begins with implementing the [gitlab-#8248](https://gitlab.com/gitlab-org/gitlab/issues/8248),
which will allow us to kick off support. Beyond the initial MVC implementation, our
focus will be on improving consistency and core features across all of our repository
types rather than providing C/C++ specific features. This may change as we begin
to get customer feedback from real-world users of the new repository.

### .NET

For .NET, the Microsoft-supported mechanism for sharing code is NuGet, which defines how packages for .NET are created, hosted, and consumed, and provides the tools for each of those roles. By integrating with NuGet, GitLab will provide a centralized location to store and view those packages, in the same place as their source code and pipelines. [gitlab-#20050](https://gitlab.com/gitlab-org/gitlab/issues/20050) details the first steps in adding NuGet support to the GitLab Package Registry.

### Linux Packages

[gitlab-#5835](https://gitlab.com/gitlab-org/gitlab/issues/5835) and [gitlab-#5932](https://gitlab.com/gitlab-org/gitlab/issues/5932), which relate to adding support for Debian and RPM respectively.

### RubyGems

[gitlab-#803](https://gitlab.com/gitlab-org/gitlab/issues/803) which will add support for a RubyGem registry.

